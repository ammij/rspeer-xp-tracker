# rspeer-xp-tracker
# RSPeer XP Tracker#
A neat GUI to track XP in RSPeer scripts. Based off Asta's snippets: http://forums.rspeer.org/topic/656/experience-tracker

![Experience image](https://gitlab.com/ammij/rspeer-xp-tracker/raw/master/xp-tracker.png)

### Usage
1. Copy `ExperienceDisplay.java`, `Experience.java` and `ColourHelper.java` in to your project

2. Have your main class implement `RenderListener` and `SkillListener`

    ```public class MyScript extends TaskScript implements RenderListener, SkillListener {```
3. Create a new `ExperienceDisplay` object:

    ```private ExperienceDisplay experienceDisplay = new ExperienceDisplay();```  
4. Call `drawSkills()` from your `RenderEvent notify()` method. 
    ```@Override
       public void notify(RenderEvent renderEvent) {
           Graphics2D g2d = (Graphics2D) renderEvent.getSource();
           experienceDisplay.drawSkills(g2d);
       }
    ``` 
   
5. Pass through the `SkillEvent` in your `SkillEvent notify()` method.

    ```@Override
       public void notify(SkillEvent skillEvent) {
           experienceDisplay.addSkill(skillEvent);
       }
   ```